package numberTheory;

public class PrimeFactors {
	public static void main(String[]args) {
		int n = 315 ; 
		
	// step 1 : Check how many times n divisible by 2 	
		while(n%2 == 0) {
			System.out.println(2+ " ");
			n/=2 ;
		}
	//step 2 : check from 3 to sqrt(n) for further divisibility
		for(int i = 3 ; i<=Math.sqrt(n); i= i+2) {
			while((n%i) == 0) {
				System.out.println(i + " ");
				n/=i;
			}
		}
	//step 3 : check for remaining n greater than 2 or not .As n doesn't get 
	//		   divisible by previous loop so it must prime number . 	
		if(n>2) 
			System.out.println(n+" ");
	}
	
	
}
