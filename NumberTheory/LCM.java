 //LCM = (a * b) /gcd(a,b) ;

package numberTheory;

public class LCM {
	public static void main(String[]args) {
		int a = 20 ;
		int b = 15;
		System.out.println("LCM of 15 and 20 is : "+lcm(a,b));
		
	}
	static int gcd(int a  ,int b) {
		if(a==0)
			return b;
		else 
			return gcd(b%a , a) ;
		
	}
	static int lcm(int a , int b) {
		return (a*b)/gcd(a,b) ;
	}
}
