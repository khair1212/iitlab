package modularArithmatic;

public class RemainderWithoutModulo {
	public static void main(String[]args) {
	    int j = 15 ;
		for(int i= 1 ; i <= 15 && j >= 1 ; i++ ) {
			System.out.println("Remainder of "+i+" % "+j+" = "+remainder(i,j));
			j--;
		}
	}
	
	static int remainder(int x , int y ) {
		return ( x -y *(x/y));
	}
}
