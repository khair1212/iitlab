// nCr = n!/r!(n-r)! 
// formula from pascal's triangle BC(n,k) = (n-1)(k-1)+(n-1)(k) ;
 
package numberTheory;

public class BinomialCoefficientOS {
	public static void main(String[]args) {
		System.out.println(BC3(6,2));
	}
	
	//Optimal Substracture causes overlapping subproblems . 
	
	static int BC(int n , int k) {		
		if(n==k || k==0)		
			return 1 ;		
		else		
			return BC(n-1,k-1)+BC(n-1,k);		
	}
	
	//solution of overlapping subproblems (DP) ; reduces overwrite 
	//iterate from top to bottom ...
	/*			1
	 		   1 1
	 		  1 2 1
	 		 1 3 3 1
	 		 	 
	
	*/
	//time : O(n*k)
	//space : O(n*k)
	static int BC2(int n , int k) {
		int c[][] = new int[n+1][k+1];
		int i = 0, j= 0 ;
		for( i = 0 ; i <= n  ;i++) {
			for( j =0 ; j <= Math.min(i, k) ; j++ ) {
				if(j == 0 || i == j ) {
					c[i][j] = 1 ;
				}else {
					c[i][j] = c[i-1][j-1] + c[i-1][j] ;
				}
			}
		}
		return c[n][k] ;
	}
	
	
	//Space optimized solution 
	//time : O(n*k)
	//space : O(k)
	
	static int BC3(int n , int k) {
		int c[] = new int[k+1];
		c[0] = 1 ;
		for(int i = 1 ; i<=n ;i++ ) {
			for(int j = Math.min(i, k) ; j > 0 ; j--) {
				c[j] = c[j-1]+c[j];
			}
		}
		return c[k] ;
		
	}
}
