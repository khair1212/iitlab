//Euler�s Totient function ?(n)  
//For number n :-> 1 to n that is relatively prime to n ..

package modularArithmatic;

public class eulersTotient {
	public static void main(String[]args) {
		 System.out.println(et2(5));
	}
	
	
	/* Efficient way solution: 
	1) Initialize result as n
	2) Consider every number 'p' (where 'p' varies from 2 to ?n). 
   	If p divides n, then do following
   	a) Subtract all multiples of p from 1 to n [all multiples of p
    will have gcd more than 1 (at least p) with n]
   	b) Update n by repeatedly dividing it by p.
	3) If the reduced n is more than 1, then remove all multiples
   	of n from result.
	*/
	static int et(int n) {
		int result = n ;
		for(int i = 2 ; i*i <=n ; i++) {
			if(n%i == 0) {
				while(n%i == 0) {
					n=n/i ;
				}
				result -= result/i;
			}
		}
		if(n>1) {
			result-= result/n ;
		}
		return result ;
	}
	
	/*Normal way : 
	 *A simple solution is to iterate through all numbers from 1 to n-1 and
	 *count numbers with gcd with n as 1. Below is the implementation of the
	 *simple method to compute Euler�s Totient function for an input integer n.
	 */
	
	static int et2(int n) {
		int result = 1 ; 
		for(int i =2 ; i <=n ; i++) {
			if(gcd(i,n) == 1) {
				result+=1 ;
			}
		}
		return result ;
	}
	static int gcd(int a , int b ) {
		if(a == 0) {
			return b ;
		}else {
			return gcd(b%a ,a );
		}
	}
}

